﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebServiceAnnonce
{
    public partial class Acceuil : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Redirect_Login(Object Sender, EventArgs E)
        {
            Response.Redirect("login.aspx");
        }
        protected void Exit_Click(Object Sender, EventArgs E)
        {
            Session["Nom"] = null;
            Session["Username"] = null;
            Session["Password"] = null;
            Response.Redirect("acceuil.aspx");
            idLogin.Visible = true;
            idLogout.Visible = false;
        }

        protected void Redirect_Page1(Object Sender, EventArgs E)
        {
            Response.Redirect("produit.aspx?id=" + idImg1.SkinID);
        }
        protected void Redirect_Page2(Object Sender, EventArgs E)
        {
            Response.Redirect("produit.aspx?id=" + idImg2.SkinID);
        }
        protected void Redirect_Page3(Object Sender, EventArgs E)
        {
            Response.Redirect("produit.aspx?id=" + idImg3.SkinID);
        }
        protected void Redirect_Page4(Object Sender, EventArgs E)
        {
            Response.Redirect("produit.aspx?id=" + idImg4.SkinID);
        }
        protected void Redirect_Page5(Object Sender, EventArgs E)
        {
            Response.Redirect("produit.aspx?id=" + idImg5.SkinID);
        }
    }
}