﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Acceuil.aspx.cs" Inherits="WebServiceAnnonce.Acceuil" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Acceuil Mon Garage</title>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"/>
    <link  rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        #zEntete{
            background-color:yellow;
            width:99%;
            position:absolute;
        }
        #zLogo{
            position:relative;
            top:2%;
            padding-top:5px;
        }
        #zMenu1{
            background-color:green;
            width:20%;
            position:absolute;
            top:1%;
            right:0%;
        }
        #zBienvenue{
            background-color:black;
            color:white;
            width:25%;
            position: absolute;
            top:25%;
            right:0%;
            display:inline-block;
        }
        #idLogin, #idLangue, #idLogout{
            text-transform:uppercase;
            color:white;
            font-size:x-large;
            text-decoration-line:none;
        }
        #zPublicite{
            background-color:red;
            width:38%;
            position:absolute;
            top:1%;
            left:30%;
            padding-left:2%;
        }
        #carrouselPub{
            width:100%;
        }
        #zPanier{
            background-color:red;
            width:10%;
            position:absolute;
            top:35%;
            right:5%;
            padding-top: 5px;
            padding-left:10px;
            padding-bottom: 5px;
            padding-right:10px;
        }
        #zBody{
            background-color:beige;
            width:99%;
            position:absolute;
            top:35%;
        }
        .item{
            background-color:whitesmoke;
            display: inline-block;
            width: 25%;
            text-align: center;
            margin: 2.2%;
            font-size: 100%;
        }
    </style>
    <script type="text/javascript">
        function changeImagePub1() {
            var listeImagePub1 = ["/Images/promo1_fr.jpg", "/Images/promo2_fr.jpg", "/Images/promo3_fr.jpg", "/Images/promo4_fr.jpg"];
            var i = 1;
            setInterval(function () {
                document.getElementById('carrouselPub').src = listeImagePub1[i++];
                if (i == 4)
                    i = 0;
            }, 2000);
        }
   
        function produitPage() {
            document.location.href="/produit.aspx";
        }

        function redirectParCat(id) {
            document.location.href = "/produit.aspx?id=" + id;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="zPage">
            <div id="zEntete">
                <div id="zLogo">
                <asp:ImageButton ImageUrl="ABC_Logo.png" AlternateText="Logo MonGarage" Width="22%" runat="server" />
                </div>
                <div id="zMenu1">
                    <asp:Button ID="idLogin" ForeColor="Gray" Text="Login" OnClick="Redirect_Login" runat="server"></asp:Button>
                    <asp:Button ID="idLogout" ForeColor="Gray"  Text="Logout" OnClick="Exit_Click" Visible="false" runat="server" ></asp:Button>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="idLangue" ForeColor="Gray"  Text="English" runat="server"></asp:Button>
                </div>
                <div id="zPublicite">
                    <img id="carrouselPub" src="/Images/promo1_fr.jpg" onclick="javascript:produitPage();" />
                </div>
                <div>
                <!--<asp:ImageButton ImageUrl="~/Images/promo1_fr.jpg" runat="server" />-->
                </div>
                <div id="zPanier">
                    Panier <asp:Image ImageUrl="~/Images/panier.png" runat="server" />
                    <asp:Label ID="qntPanier" Text="0" runat="server"></asp:Label>
                    <asp:Label ID="totPanier" Text="$ 0.00" runat="server"></asp:Label>
                </div>
                <div id="zBienvenue">
                    <asp:Label ID="lblBienvenue" Text="Bienvenue : " runat="server" />
                    <asp:Label ID="lblUtilisateur" runat="server" />                    
                </div>
            </div>
            <div id="zBody">
            <p>
                <asp:Label ID="Label1" runat="server"></asp:Label>
            </p>
                <div class="item">
                    <asp:ImageButton ID="idImg1" SkinID="1" class="imageItem" AlternateText="Categorie 1" OnClick="Redirect_Page1" runat="server" />
                    <div class="description"><asp:Label AssociatedControlID="idImg1" ID="idCat1" runat="server"></asp:Label></div> //location.assign(URL)
                </div>
                <div class="item">
                    <asp:ImageButton ID="idImg2" SkinID="2" class="imageItem" AlternateText="Categorie 2" OnClick="Redirect_Page2" runat="server" />
                    <div class="description"><asp:Label AssociatedControlID="idImg2" ID="idCat2" runat="server"></asp:Label></div>
                </div>
                <div class="item">
                    <asp:ImageButton ID="idImg3" SkinID="3" class="imageItem" AlternateText="Categorie 3" OnClick="Redirect_Page3" runat="server" />
                    <div class="description"><asp:Label AssociatedControlID="idImg3" ID="idCat3" runat="server"></asp:Label></div>
                </div>
                <div class="item">
                    <asp:ImageButton ID="idImg4" SkinID="4" class="imageItem" AlternateText="Categorie 4" OnClick="Redirect_Page4" runat="server" />
                    <div class="description"><asp:Label AssociatedControlID="idImg4" ID="idCat4" runat="server"></asp:Label></div>
                </div>
                <div class="item">
                    <asp:ImageButton ID="idImg5" SkinID="5" class="imageItem" AlternateText="Categorie 5" OnClick="Redirect_Page5" runat="server" />
                    <div class="description"><asp:Label AssociatedControlID="idImg5" ID="idCat5" runat="server"></asp:Label></div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
