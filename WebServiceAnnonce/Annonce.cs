﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebServiceAnnonce
{
    public class Annonce
    {
        private int id;
        private string titre;
        private string description;
        private string datePublication;
        private string categorie;
        private static string msgConfirmation;

        public int Id { get => id; set => id = value; }
        public string Titre { get => titre; set => titre = value; }
        public string Description { get => description; set => description = value; }
        public string DatePublication { get => datePublication; set => datePublication = value; }
        public string Categorie { get => categorie; set => categorie = value; }
        public static string MsgConfirmation { get => msgConfirmation; set => msgConfirmation = value; }

        public Annonce() { }
        public Annonce(string titre, string description, string datePublication, string categorie)
        {
            this.titre = titre;
            this.description = description;
            this.datePublication = datePublication;
            this.categorie = categorie;
        }
        public Annonce(int id, string titre, string description, string datePublication, string categorie)
        {
            this.id = id;
            this.titre = titre;
            this.description = description;
            this.datePublication = datePublication;
            this.categorie = categorie;
        }
        public int Enregistrer()
        {
            System.Data.SqlClient.SqlConnection con;
            try
            {
                con = new System.Data.SqlClient.SqlConnection();
                con.ConnectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;";
                con.ConnectionString += "AttachDbFilename = c:\\users\\1895434\\source\\repos\\WebServiceAnnonce\\WebServiceAnnonce\\App_Data\\Database1.mdf;";
                con.ConnectionString += "Integrated Security=True;Connect Timeout=30";
                con.Open();
                msgConfirmation = "Connexion OK !";
                string requete = "insert into annonce (titre, description, datePublication, categorie) values ('"
                + titre + "', '" + description + "', '" + datePublication + "', '" + categorie + "');";
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand sqlCommand = new SqlCommand(requete, con);
                adapter.InsertCommand = sqlCommand;
                adapter.InsertCommand.ExecuteNonQuery();
                sqlCommand.Dispose();
                //msgConfirmation = "Une nouvelle annonce est ajoutée";
                con.Close();
                return 1;
            }
            catch (ArgumentException ex)
            {
                //msgConfirmation = ex.Message;
                return 0;
            }

        }

        public static List<Annonce> GetListeAnnonces()
        {
            List<Annonce> liste = new List<Annonce>();
            System.Data.SqlClient.SqlConnection con;
            try
            {
                con = new System.Data.SqlClient.SqlConnection();
                con.ConnectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;";
                con.ConnectionString += "AttachDbFilename = c:\\users\\1895434\\source\\repos\\WebServiceAnnonce\\WebServiceAnnonce\\App_Data\\Database1.mdf;";
                con.ConnectionString += "Integrated Security=True;Connect Timeout=30";
                con.Open();
                msgConfirmation = "Connexion OK !";
                string requete = "select * from annonce";
                SqlCommand commandeSQL = new SqlCommand(requete, con);
                SqlDataReader resultat = commandeSQL.ExecuteReader();

                while (resultat.Read())
                {
                    int id = resultat.GetInt32(0);
                    string titre = resultat.GetString(1);
                    string description = resultat.GetString(2);
                    string datePublication = resultat.GetString(3);
                    string categorie = resultat.GetString(4);
                    Annonce an = new Annonce(id, titre, description, datePublication, categorie);
                    liste.Add(an);
                }
            }
            catch (ArgumentException ex)
            {
                msgConfirmation = ex.Message;
            }
            return liste;
        }

        override
        public string ToString()
        {
            return id + " " + titre + " " + description + " " + datePublication + " " + categorie;
        }
    }

}