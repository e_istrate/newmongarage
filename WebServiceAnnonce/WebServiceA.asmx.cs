﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;

namespace WebServiceAnnonce
{
    /// <summary>
    /// Description résumée de WebServiceA
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Pour autoriser l'appel de ce service Web depuis un script à l'aide d'ASP.NET AJAX, supprimez les marques de commentaire de la ligne suivante. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServiceA : System.Web.Services.WebService
    {

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void EnregistrerAnnonce(string titre, string description, string datePublication, string categorie)
        {
            Annonce annonce = new Annonce(titre, description, datePublication, categorie);
            int reponse = annonce.Enregistrer();
            JavaScriptSerializer js = new JavaScriptSerializer();
            if (reponse == 1)
            {
                Context.Response.Write(js.Serialize(annonce));
            }
            else
            {
                Context.Response.Write(js.Serialize(reponse));
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void ListeAnnonce(){
            List<Annonce> liste = Annonce.GetListeAnnonces();

            for (int i = 0; i < liste.Count; i++)
            {
                new Annonce()
                {
                    Id = liste[i].Id,
                    Titre = liste[i].Titre,
                    Description = liste[i].Description,
                    DatePublication = liste[i].DatePublication,
                    Categorie = liste[i].Categorie
                };
            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Write(js.Serialize(liste));
        } 
    }
}
