﻿CREATE TABLE [dbo].[annonce]
(
	[Id] INT NOT NULL PRIMARY KEY identity, 
    [titre] VARCHAR(50) NULL, 
    [description] VARCHAR(255) NULL, 
    [datePublication] VARCHAR(50) NULL, 
    [categorie] VARCHAR(50) NULL
)
